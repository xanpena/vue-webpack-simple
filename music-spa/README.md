# music-spa

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).

# Memoria de Desarrollo

## Requisitos

- Node.js
- Npm

# Ejecución

1. Utilizaremos webpack-simple
https://github.com/vuejs-templates/webpack-simple

``` bash
# instalamos vue-cli
npm install -g vue-cli

vue init webpack-simple music-spa

cd music-spa

npm install

npm run dev

```

2. Incorporamos Stylus y Pug
http://stylus-lang.com/
https://pugjs.org/api/getting-started.html

``` bash
npm install --save-dev pug pug-loader stylus stylus-loader

# Si pug da problemas...
npm i -D pug
```


### Utilidades 
https://html2jade.org/
http://beautifytools.com/css-to-stylus-converter.php

3. Usaremos la API de last.fm para obtener listas de artistas

https://www.last.fm/api/?lang=es&